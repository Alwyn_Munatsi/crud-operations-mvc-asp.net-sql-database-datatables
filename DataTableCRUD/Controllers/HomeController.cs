﻿using DataTableCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DataTableCRUD.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployees()
        {
            using (CrudDatabaseEntities dc = new CrudDatabaseEntities())
            {
                var employees = dc.Employees.OrderBy(a => a.FirstName).ToList();
                return Json(new { data = employees }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult Save(int id)
        {
            using (CrudDatabaseEntities dc = new CrudDatabaseEntities())
            {
                var v = dc.Employees.Where(a => a.EmployeeId == id).FirstOrDefault();
                return View(v);
            }
        }

        [HttpPost]
        public ActionResult Save(Employee emp)
        {
            bool status = false;
            if (ModelState.IsValid)
            {
                using (CrudDatabaseEntities dc = new CrudDatabaseEntities())
                {
                    if (emp.EmployeeId > 0)
                    {
                        //Edit
                        var v = dc.Employees.Where(a => a.EmployeeId == emp.EmployeeId).FirstOrDefault();
                        if(v != null)
                        {
                            v.FirstName = emp.FirstName;
                            v.LastName = emp.LastName;
                            v.EmailID = emp.EmailID;
                            v.City = emp.City;
                            v.Country = emp.Country;
                        }
                    }
                    else
                    {
                        //Save
                        dc.Employees.Add(emp);
                    }
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (CrudDatabaseEntities dc = new CrudDatabaseEntities())
            {
                var employees = dc.Employees.Where(a => a.EmployeeId==id).ToList();
                if(employees != null)
                {
                    return View(employees);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteEmployee(int id)
        {
            bool status = false;
            using (CrudDatabaseEntities dc = new CrudDatabaseEntities())
            {
                var v = dc.Employees.Where(a => a.EmployeeId == id).ToList();
                if (v != null)
                {
                    dc.Employees.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}